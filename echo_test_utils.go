package test_utils_go

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
)

func DoTestRequest(method, path string, app http.Handler, buf *bytes.Buffer) (status int, body string) {
	rec := DoTestRequestWithResponse(method, path, app, map[string]string{}, buf)
	if rec == nil {
		return 0, ""
	}

	status, body = rec.Code, rec.Body.String()
	return status, body
}

func DoTestRequestWithHeaders(method, path string, app http.Handler, headers map[string]string, buf *bytes.Buffer) (status int, body string) {
	rec := DoTestRequestWithResponse(method, path, app, headers, buf)
	if rec == nil {
		return 0, ""
	}

	status, body = rec.Code, rec.Body.String()
	return status, body
}

func DoTestRequestWithResponse(method, path string, app http.Handler, headers map[string]string, buf *bytes.Buffer) *httptest.ResponseRecorder {
	if buf == nil {
		buf = bytes.NewBufferString("")
	}

	req, err := http.NewRequest(method, path, buf)
	if err != nil {
		log.Printf("could not create request, error: %v", err)
		return nil
	}

	for k, v := range headers {
		req.Header.Set(k, v)
	}

	rec := httptest.NewRecorder()

	app.ServeHTTP(rec, req)

	return rec
}

func ReadFixture(file string) string {
	d, _ := ioutil.ReadFile(fmt.Sprintf("tests-data/%s", file))
	return string(d)
}
